#include <bird.hpp>
#include <iostream>

namespace mt
{
	Bird::Bird(int x0, int y0, float r, float angle, float v0)
	{
		m_x0 = x0;
		m_y0 = y0;
		m_r = r;
		m_angle = (2 * acos(-1) - angle);
		m_v0 = v0;
	}

	bool Bird::Setup()
	{
		if (!m_texture.loadFromFile("img/bird.png"))
		{
			std::cout << "ERROR when loading bird.png" << std::endl;
			return false;
		}

		m_bird = new sf::Sprite();
		m_bird->setTexture(m_texture);
		m_bird->setOrigin(m_r, m_r);
		m_bird->setPosition(m_x, m_y);

		return true;
	}

	Bird::~Bird()
	{
		if (m_bird != nullptr)
			delete m_bird;
	}

	void Bird::Move(float t)
	{
		m_x = m_x0 + m_v0 * cos(m_angle) * t;
		m_y = m_y0 + m_v0 * sin(m_angle) * t + g * t * t / 2;
		m_bird->setPosition(m_x, m_y);
	}

	sf::Sprite* Bird::Get() { return m_bird; }

	int Bird::GetX() { return m_x; }
	int Bird::GetY() { return m_y; }
	float Bird::GetR() { return m_r; }
}